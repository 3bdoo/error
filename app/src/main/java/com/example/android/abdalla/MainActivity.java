package com.example.android.abdalla;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    // Making a list of type Person, and naming it persons
    private List<Person> persons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Getting the RecyclerView from our activity
        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);

        rv.setHasFixedSize(true); //since our list has a fixed element size, we use this method to improve performance

        //we call this method to add our data into the list
        initializeData();

        LinearLayoutManager llm = new LinearLayoutManager(this); //adding the LinearLayoutManager and passing the context
        rv.setLayoutManager(llm); // setting the LinearLayoutManager to our RecyclerView
        RVAdapter adapter = new RVAdapter(persons); //Making new object of our Adapter and send the list to it
        rv.setAdapter(adapter); // setting the Adapter to our RecyclerView

    }


    /**
     * This method is to add data to the persons list
     */
    private void initializeData(){
        persons = new ArrayList<>();
        persons.add(new Person("Emma Wilson", "23 years old", R.drawable.emma));
        persons.add(new Person("Lavery Maiss", "25 years old", R.drawable.lavery));
        persons.add(new Person("Lillie Watts", "35 years old", R.drawable.lillie));
    }
}
